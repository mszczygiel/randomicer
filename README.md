# Randomicer
## Downloads
[randomicer-v0.1.jar](/uploads/92631ccfacf2d5bad1da26be85f5a278/randomicer-v0.1.jar)
### Description
This software performs randomization of mice based on tumor size
### Audience
Scientists who want to randomize mice easier
### Requirements
Java JRE 8 installed
### Input file format
Each pair of lines represents single measurement (possibly with multiple tumors).
First line of each pair starts with mouse id, followed by separator (;) and at least one measurement.
Second line starts with separator, and the same number of measurements as in the first line

**Example input file**
```
1;5.04
;6.68
2;2.42
;2.51
3;5.78
;6.75
4;3.88;1.1
;5.12;1.2
```
### Usage
`java -jar randomicer.jar -f data.csv -g 3 -m 6 -o output.csv -tl 4 -tu 200`
where

`-f <input-filename>` - path to an input file. If not present, then read from standard input

`-g <x>` - number of groups

`-m <x>` - number of mice per group

`-tl <x>` - tumor lower threshold (default=0). mice with total tumor volume < x will not be considered for randomization

`-tu <x>` - tumor upper threshold (default=999999). mice with total tumor volume > x will not be considered for randomization

`-o <output-filename>` output file name. **Please note, that if given file already exists it will be overwritten**
