name := "randomicer"

version := "1.0"

scalaVersion := "2.12.3"

libraryDependencies += "org.typelevel" %% "cats" % "0.9.0"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"

scalacOptions ++= Seq("-deprecation", "-feature", "-language:higherKinds")

        