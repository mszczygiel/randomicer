package com.mszczygiel.randomicer.input

import java.io.StringReader

import com.mszczygiel.randomicer.data.{Mouse, Tumor}
import org.scalatest.{FlatSpec, Matchers}

import scala.util.Success

class TryReadMiceRandomizationSpecificationTest extends FlatSpec with Matchers {
  import cats.implicits._

  behavior of "readInput"

  it should "read from console" in {
    Console.withIn(new StringReader(
      """
        |1;2;3
        |;3;4
        |2;6
        |;5
        |
      """.stripMargin)) {

      val input = ReadInput.readSpecification(Args(Map("m" -> "1", "g" -> "3", "tl" -> "2.12", "tu" -> "531.5"))).foldMap(TryReadInput.interpret())

      val expectedSettings = RandomizationSettings(3, 1, 2.12, 531.5)
      val expectedMice = Seq(
        Mouse(1, List(Tumor(2, 3), Tumor(3, 4))),
        Mouse(2, List(Tumor(6, 5)))
      )
      assert (input === Success(RandomizationSpecification(expectedSettings, expectedMice)))
    }
  }
}
