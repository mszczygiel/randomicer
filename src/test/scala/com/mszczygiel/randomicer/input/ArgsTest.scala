package com.mszczygiel.randomicer.input

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Success

class ArgsTest extends FlatSpec with Matchers {

  behavior of "Args"

  it should "parse arguments and flags" in {
    val args = Args.parse(List("--a-flag", "-progname", "randomicer", "-version", "2"))
    assert(args.getString("progname") === Some("randomicer"))
    assert(args.flagPresent("a-flag") === true)
    assert(args.getInt("version") === Success(Some(2)))
  }

  behavior of "getInt"

  it should "return Success(None) when args is empty" in {
    assert(Args.parse(List()).getInt("x") === Success(None))
  }

  it should "return Success(Some(value)) when arg is given an int value" in {
    assert(Args.parse(List("-argname", "-124")).getInt("argname") === Success(Some(-124)))
  }

  behavior of "getDouble"

  it should "return Success(None) when args is e mpty" in {
    assert(Args.parse(List()).getDouble("x") === Success(None))
  }

  it should "return Success(Some(value)) when arg is given a double value" in {
    assert(Args.parse(List("-argname", "-124.72")).getDouble("argname") === Success(Some(-124.72)))
  }


  behavior of "getString"

  it should "return None when args is empty" in {
    assert(Args.parse(List()).getString("x") === None)
  }

  it should "return a value when it's name was prepended with -" in {
    assert(Args.parse(List("-name", "this is a value")).getString("name") === Some("this is a value"))
  }

  it should "omit a value when only option name is provided" in {
    assert(Args.parse(List("-name")).getString("name") === None)
  }

  behavior of "flagPresent"

  it should "be false when args is empty" in {
    assert(Args.parse(List()).flagPresent("x") === false)
  }

  it should "be true only when given flag is prepended with --" in {
    val args = Args.parse(List("--simple-flag", "-not-a-flag"))
    assert(args.flagPresent("simple-flag") === true)
    assert(args.flagPresent("not-a-flag") === false)
  }
}
