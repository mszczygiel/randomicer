package com.mszczygiel.randomicer.randomization

import com.mszczygiel.randomicer.data.{Mouse, Tumor}
import com.mszczygiel.randomicer.input.{RandomizationSettings, RandomizationSpecification}
import org.scalatest.{FlatSpec, Matchers}

class SimpleRandomAlgorithmTest extends FlatSpec with Matchers with SimpleRandomAlgorithm {
  behavior of "randomize"

  it should "return randomization with penalty <= 4 for tumor with volumes 1-9" in {
    val spec = RandomizationSpecification(
      RandomizationSettings(3, 3, 0.0, 100),
      List(
        Mouse(1, List(Tumor(1, 2))),
        Mouse(2, List(Tumor(1, 4))),
        Mouse(3, List(Tumor(1, 6))),
        Mouse(4, List(Tumor(1, 8))),
        Mouse(5, List(Tumor(1, 10))),
        Mouse(6, List(Tumor(1, 12))),
        Mouse(7, List(Tumor(1, 14))),
        Mouse(8, List(Tumor(1, 16))),
        Mouse(9, List(Tumor(1, 18)))
      )
    )
    val distribution = randomize(spec)

    assert(distribution.penalty <= 4.0)
  }

  it should "not consider tumors outside of limits" in {
    val spec = RandomizationSpecification(
      RandomizationSettings(2, 2, 4.0, 50),
      List(
        Mouse(1, List(Tumor(1, 2))),
        Mouse(2, List(Tumor(1, 4))),
        Mouse(3, List(Tumor(1, 8))),
        Mouse(4, List(Tumor(1, 10))),
        Mouse(5, List(Tumor(1, 100000))),
      )
    )

    val distribution = randomize(spec)

    val usedMice = distribution.groups.flatMap(g => g.mice.toList).map(_.id)
    val groupSizes = distribution.groups.map(g => g.mice.toList.size)

    groupSizes should have size 2
    groupSizes should contain only(1)
    usedMice should contain only(3, 4)
  }
}
