package com.mszczygiel.randomicer.statistics

import cats.data.NonEmptyList
import com.mszczygiel.randomicer.statistics.StatisticsFun.{mean, median, variance, standardDeviation}
import org.scalatest.{FlatSpec, Matchers}

class StatisticsFunTest extends FlatSpec with Matchers {
  private val Tolerance = 0.001

  behavior of "a mean"

  it should "return an element from single element list" in {
    assert(mean(NonEmptyList(5.5, Nil)) === 5.5 +- Tolerance)
  }

  it should "return a mean from a list" in {
    assert(mean(NonEmptyList(1.0, List(5.0, -1.0, 7.5))) === 3.125 +- Tolerance)
  }


  behavior of "a median"

  it should "return an element from single element list" in {
    assert(median(NonEmptyList(5.5, Nil)) === 5.5 +- Tolerance)
  }

  it should "return middle element from odd sized list" in {
    assert(median(NonEmptyList(-3, List(10, -5, -2.5, -3.1))) === -3.0 +- Tolerance)
  }

  it should "return average of two middle elements for even sized list" in {
    assert(median(NonEmptyList(-3, List(10, -5, -2.5, -3.1, 7.3))) === -2.75 +- Tolerance)
  }

  behavior of "a variance"

  it should "return 0 for single element list" in {
    assert(variance(NonEmptyList(123, Nil)) === 0.0 +- Tolerance)
  }

  it should "return a variance for example list" in {
    assert(variance(NonEmptyList(600, List(470, 170, 430, 300))) === 21704.0 +- Tolerance)
  }

  behavior of "a standard deviation"

  it should "return 0 for single element list" in {
    assert(standardDeviation(NonEmptyList(123, Nil)) === 0.0 +- Tolerance)
  }

  it should "return a standard deviation for example list" in {
    assert(standardDeviation(NonEmptyList(600, List(470, 170, 430, 300))) === 147.322 +- Tolerance)
  }
}
