package com.mszczygiel.randomicer.data

import org.scalatest.{FlatSpec, Matchers}

class TumorTest extends FlatSpec with Matchers {
  behavior of "volume"

  it should "fail create when first dimension is negative" in {
    assertThrows[IllegalArgumentException] {
      Tumor(-1.0, 1)
    }
  }

  it should "fail create when second dimension is negative" in {
    assertThrows[IllegalArgumentException] {
      Tumor(1.0, -1.0)
    }
  }

  it should "be 0" in {
    assert(Tumor(0, 0).volume === (0.0 +- 0.001))
  }

  it should "be calculated correctly" in {
    assert(Tumor(1.2, 3.6).volume === (2.592 +- 0.001))
  }
}
