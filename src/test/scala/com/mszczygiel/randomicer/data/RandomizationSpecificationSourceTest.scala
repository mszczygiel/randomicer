package com.mszczygiel.randomicer.data

import java.nio.file.Paths

import com.mszczygiel.randomicer.input.{Args, ConsoleSource, FileSource, InputSource}
import org.scalatest.{FlatSpec, Matchers}

class RandomizationSpecificationSourceTest extends FlatSpec with Matchers {
  behavior of "determineInputSource"

  it should "return Console from empty arguments list" in {
    assert(InputSource.determineSource(Args()) === ConsoleSource)
  }

  it should "return file with name when -f is followed by the name" in {
    assert(InputSource.determineSource(Args(Map("f" -> "file path"))) === FileSource(Paths.get("file path")))
  }

}
