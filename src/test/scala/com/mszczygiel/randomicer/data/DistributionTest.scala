package com.mszczygiel.randomicer.data

import cats.data.NonEmptyList
import org.scalatest.{FlatSpec, Matchers}

class DistributionTest extends FlatSpec with Matchers {
  behavior of "penalty"

  it should "return 0 for empty distribution" in {
    assert(Distribution(Seq()).penalty === (0.0 +- 0.001))
  }

  it should "return sum of absolute differences between medians and means" in {
    val distribution = Distribution(Seq(
      Group(NonEmptyList(Mouse(1, Seq(Tumor(1, 2))), Nil)),
      Group(NonEmptyList(Mouse(2, Seq(Tumor(3, 1))), Nil))
    ))
    assert(distribution.penalty === (1.0 +- 0.001))
  }
}
