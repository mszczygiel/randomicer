package com.mszczygiel.randomicer

import com.mszczygiel.randomicer.input.{Args, ReadInput, TryReadInput}
import com.mszczygiel.randomicer.output.{ConsoleOutput, CsvFileOutput}
import com.mszczygiel.randomicer.randomization.{Randomicer, TryRandomizer}

import scala.util.{Failure, Success}

object Application extends App {

  import cats.implicits._

  val parsedArgs: Args = Args.parse(args.toList)
  val tryInput = ReadInput.readSpecification(parsedArgs).foldMap(TryReadInput.interpret())
  val tryDistribution = tryInput.flatMap { input =>
    Randomicer.distibuteMice(input).foldMap(TryRandomizer.interpret())
  }

  tryDistribution match {
    case Failure(e) => System.err.println(e)
    case Success(distribution) =>
      val outputStrategy = parsedArgs.getString("o")
      outputStrategy match {
        case None => ConsoleOutput.printDistribution(distribution)
        case Some(path) => CsvFileOutput.writeDistribution(path, distribution)
      }
  }
}
