package com.mszczygiel.randomicer.input

import scala.annotation.tailrec
import scala.util.Try

class Args (private[this] val valueOptions: Map[String, String], private[this] val toggleOptions: Set[String]) {
  def getInt(name: String): Try[Option[Int]] = {
    Try {
      valueOptions.get(name).map(_.toInt)
    }
  }
  def getString(name: String): Option[String] = {
    valueOptions.get(name)
  }

  def getDouble(name: String): Try[Option[Double]] = {
    Try {
      valueOptions.get(name).map(_.toDouble)
    }
  }

  def flagPresent(name: String): Boolean = {
    toggleOptions contains name
  }
}

object Args {
  def apply(valueOptions: Map[String, String] = Map(), toggleOptions: Set[String] = Set()): Args = new Args(valueOptions, toggleOptions)

  def parse(args: List[String]): Args = {
    var valueOpts = Map.newBuilder[String, String]
    var toggleOpts = Set.newBuilder[String]

    @tailrec
    def interpretArgs(args: List[String]): Unit = args match {
      case opt::value::rest if opt.startsWith("-") && !opt.startsWith("--") =>
        valueOpts += opt.substring(1) -> value
        interpretArgs(rest)
      case opt::rest if opt.startsWith("--") =>
        toggleOpts += opt.substring(2)
        interpretArgs(rest)
      case _::rest => interpretArgs(rest)
      case _ => ()
    }

    interpretArgs(args)

    new Args(valueOpts.result(), toggleOpts.result())
  }
}
