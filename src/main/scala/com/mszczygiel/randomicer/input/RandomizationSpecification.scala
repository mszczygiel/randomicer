package com.mszczygiel.randomicer.input

import com.mszczygiel.randomicer.data.Mouse

case class RandomizationSettings(numberOfGroups: Int,
                                 numberOfMicePerGroup: Int,
                                 tumorLowerThreshold: Double,
                                 tumorUpperThreshold: Double)

case class RandomizationSpecification(settings: RandomizationSettings, mice: Seq[Mouse])
