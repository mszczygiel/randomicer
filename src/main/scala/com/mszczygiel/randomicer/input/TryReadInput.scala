package com.mszczygiel.randomicer.input

import java.nio.file.{Files, Path}
import java.text.{DecimalFormat, DecimalFormatSymbols}

import cats.~>
import com.mszczygiel.randomicer.data.{Mouse, Tumor}

import scala.io.{Source, StdIn}
import scala.util.Try

object TryReadInput {
  private val Separator = ";"

  def interpret(): ReadInputOp ~> Try = new (ReadInputOp ~> Try) {
    val decimalFormat = new DecimalFormat()
    val decimalFormatSymbols = new DecimalFormatSymbols()
    decimalFormatSymbols.setDecimalSeparator('.')
    decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols)

    override def apply[A](fa: ReadInputOp[A]): Try[A] = {
      fa match {
        case ReadMice(source) => source match {
          case ConsoleSource => parseLines(consoleLines)
          case FileSource(path) => parseLines(fileLines(path))
        }
        case ReadSettings(args) => readSettings(args)
      }
    }

    private def readSettings(args: Args): Try[RandomizationSettings] = {
      Try {
        val groups = args.getInt("g").get
        val micePerGroup = args.getInt("m").get
        val tumorLowerThreshold = args.getDouble("tl").get
        val tumorUpperThreshold = args.getDouble("tu").get
        if(groups.isEmpty) throw new IllegalArgumentException("please define number of groups with -g option")
        if(micePerGroup.isEmpty) throw new IllegalArgumentException("please define number of mice per group with -m option")

        RandomizationSettings(groups.get, micePerGroup.get, tumorLowerThreshold.getOrElse(0), tumorUpperThreshold.getOrElse(999999))
      }
    }

    private def consoleLines: List[String] = Stream.continually(StdIn.readLine()).takeWhile(_ != null).filter(_.trim.nonEmpty).toList

    private def fileLines(path: Path): List[String] = Source.fromFile(path.toFile).getLines().toList

    private def parseLines(lines: List[String]): Try[List[Mouse]] = Try {
      if (lines.length %2 != 0) throw new IllegalArgumentException("Number of lines must be even")

      val mice = lines.grouped(2).map{
        case l1::l2::Nil =>
          parseMouse(l1, l2)
        case _ => throw new IllegalArgumentException("Could not match pair of tumor dimensions")
      }

      mice.toList
    }

    private def parseMouse(l1: String, l2: String) = {
      val l1Splitted = l1.split(Separator)
      val l2Splitted = l2.split(Separator)
      if (l1Splitted.length < 2) throw new IllegalArgumentException("Each line must have at least 2 columns")
      if (l2Splitted.length < 2) throw new IllegalArgumentException("Each line must have at least 2 columns")
      if (l1Splitted.length != l2Splitted.length) throw new IllegalArgumentException("Subsequent pairs of lines must have the same number of columns")
      if (l2Splitted.head.trim.length > 0) throw new IllegalArgumentException(s"First column of a 2nd line of a pair ${l2Splitted.head} should be empty")
      val mouseId = l1Splitted(0).toInt
      val tumors = l1Splitted.tail.zip(l2Splitted.tail).map { case (dim1, dim2) =>
        Tumor(decimalFormat.parse(dim1).doubleValue(), decimalFormat.parse(dim2).doubleValue())
      }
      Mouse(mouseId, tumors.toList)
    }
  }

}
