package com.mszczygiel.randomicer.input

import java.nio.file.{Path, Paths}

import cats.Monad
import com.mszczygiel.randomicer.data.Mouse

import scala.annotation.tailrec

sealed trait InputSource
case object ConsoleSource extends InputSource
case class FileSource(path: Path) extends InputSource

sealed trait ReadInputOp[A]
case class ReadMice(source: InputSource) extends ReadInputOp[List[Mouse]]
case class ReadSettings(args: Args) extends ReadInputOp[RandomizationSettings]

object InputSource {
  def determineSource[M[_]: Monad](args: Args): M[InputSource] = {
    val FileOptionName = "f"
    Monad[M].pure(args.getString(FileOptionName).map(path => FileSource(Paths.get(path))).getOrElse(ConsoleSource))
  }
}
