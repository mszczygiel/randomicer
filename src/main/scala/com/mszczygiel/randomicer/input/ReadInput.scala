package com.mszczygiel.randomicer.input

import cats.Id
import cats.free.Free
import cats.free.Free.liftF
import com.mszczygiel.randomicer.data.Mouse

object ReadInput {
  type ReadInput[A] = Free[ReadInputOp, A]

  def readSpecification(args: Args): ReadInput[RandomizationSpecification] = {
    for {
      settings <- readSettings(args)
      mice <- readMice(args)
    } yield RandomizationSpecification(settings, mice)
  }

  private def readSettings(args: Args): ReadInput[RandomizationSettings] = {
    liftF[ReadInputOp, RandomizationSettings](ReadSettings(args))
  }

  private def readMice(args: Args): ReadInput[List[Mouse]] = {
    val source = InputSource.determineSource[Id](args)
    liftF[ReadInputOp, List[Mouse]](ReadMice(source))
  }
}
