package com.mszczygiel.randomicer.randomization

import cats.data.NonEmptyList
import com.mszczygiel.randomicer.data.{Distribution, Group}
import com.mszczygiel.randomicer.input.RandomizationSpecification

import scala.util.Random

trait SimpleRandomAlgorithm {

  import SimpleRandomAlgorithm._

  def randomize(spec: RandomizationSpecification): Distribution = {
    randomDistributions(spec).take(NumberOfIterations).minBy(_.penalty)
  }

  private def randomDistributions(spec: RandomizationSpecification): Stream[Distribution] = {
    val random = new Random(0L)

    def nextDistribution(): Distribution = {
      val mice = random.shuffle(spec.mice)
        .filter(_.tumorVolume >= spec.settings.tumorLowerThreshold)
        .filter(_.tumorVolume <= spec.settings.tumorUpperThreshold)
        .take(spec.settings.numberOfGroups * spec.settings.numberOfMicePerGroup)
      val groups = mice
        .zipWithIndex
        .groupBy {
          case (_, idx: Int) => idx % spec.settings.numberOfGroups
        }
        .values
        .map(_.map(_._1))
        .take(spec.settings.numberOfGroups)
        .flatMap(m => NonEmptyList.fromList(m.toList))
        .map(m => Group(m))
      Distribution(groups.toSeq)
    }

    Stream.continually(nextDistribution())
  }
}

object SimpleRandomAlgorithm {
  private val NumberOfIterations = 1000
}
