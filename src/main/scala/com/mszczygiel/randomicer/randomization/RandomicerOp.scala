package com.mszczygiel.randomicer.randomization

import com.mszczygiel.randomicer.data.Distribution
import com.mszczygiel.randomicer.input.RandomizationSpecification

sealed trait RandomicerOp[A]
case class Randomize(input: RandomizationSpecification) extends RandomicerOp[Distribution]
