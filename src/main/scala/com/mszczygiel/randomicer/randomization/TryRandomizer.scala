package com.mszczygiel.randomicer.randomization

import cats.{Monad, ~>}

import scala.util.Try

object TryRandomizer extends SimpleRandomAlgorithm {

  import cats.implicits._

  def interpret(): RandomicerOp ~> Try = new (RandomicerOp ~> Try) {
    override def apply[A](fa: RandomicerOp[A]): Try[A] = fa match {
      case Randomize(spec) =>
        Monad[Try].pure(randomize(spec))
    }
  }
}
