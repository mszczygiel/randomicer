package com.mszczygiel.randomicer.randomization

import cats.free.Free
import cats.free.Free.liftF
import com.mszczygiel.randomicer.data.Distribution
import com.mszczygiel.randomicer.input.RandomizationSpecification

object Randomicer {
  type Randomicer[A] = Free[RandomicerOp, A]

  def distibuteMice(input: RandomizationSpecification): Randomicer[Distribution] = randomize(input)

  private def randomize(input: RandomizationSpecification): Randomicer[Distribution] = {
    liftF[RandomicerOp, Distribution](Randomize(input))
  }
}

