package com.mszczygiel.randomicer.statistics

import cats.data.NonEmptyList
import cats.implicits._

import scala.math._

object StatisticsFun {
  def mean(vals: NonEmptyList[Double]): Double = {
    vals.reduce / sizeOfNel(vals)
  }

  def median(vals: NonEmptyList[Double]): Double = {
    val list = vals.toList.sorted
    val size = sizeOfNel(vals)
    if (size % 2 == 0) 0.5 * (list(size / 2 - 1) + list(size / 2))
    else list(size / 2)
  }

  def variance(vals: NonEmptyList[Double]): Double = {
    val avg = mean(vals)
    vals.foldLeft(0.0) {
      case (sum, v) => sum + pow(v - avg, 2)
    } / sizeOfNel(vals)
  }

  def standardDeviation(vals: NonEmptyList[Double]): Double = {
    math.sqrt(variance(vals))
  }

  private[this] def sizeOfNel(nel: NonEmptyList[_]): Int = {
    nel.tail.size + 1
  }
}
