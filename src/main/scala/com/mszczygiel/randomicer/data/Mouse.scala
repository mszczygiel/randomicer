package com.mszczygiel.randomicer.data

import cats.Show


case class Tumor private(firstDimension: Double, secondDimension: Double) {
  val volume: Double = {
    val longer = Math.max(firstDimension, secondDimension)
    val shorter = Math.min(firstDimension, secondDimension)
    (shorter * shorter) * (0.5 * longer)
  }
}

case class Mouse(id: Int, tumors: Seq[Tumor]) {
  val tumorVolume: Double = tumors.map(_.volume).sum
}

object Mouse {
  implicit val showMouse: Show[Mouse] = Show.show[Mouse](mouse => s"${mouse.id.toString} - " + f"${mouse.tumorVolume}%1.2f")
}

object Tumor {
  def apply(firstDimension: Double, secondDimension: Double): Tumor = {
    val NegativeDimensionError = "Dimension must be >= 0"
    require(firstDimension >= 0, NegativeDimensionError)
    require(secondDimension >= 0, NegativeDimensionError)
    new Tumor(firstDimension, secondDimension)
  }
}

