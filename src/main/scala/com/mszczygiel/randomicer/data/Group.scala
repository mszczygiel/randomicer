package com.mszczygiel.randomicer.data

import cats.Show
import cats.data.NonEmptyList
import com.mszczygiel.randomicer.statistics.StatisticsFun

case class Group(mice: NonEmptyList[Mouse]) {
  def median: Double = StatisticsFun.median(volumes)
  def mean: Double = StatisticsFun.mean(volumes)
  def standardDeviation: Double = StatisticsFun.standardDeviation(volumes)
  def variance: Double = StatisticsFun.variance(volumes)

  private def volumes: NonEmptyList[Double] = mice.map(_.tumorVolume)
}

object Group {
  implicit def showGroup(implicit showMouse: Show[Mouse]): Show[Group] =
    (group: Group) =>
      f"AVG(${group.mean}%.2f) MED(${group.median}%.2f) SD(${group.standardDeviation}%.2f) --- " +
        group.mice.toList.sortBy(_.tumorVolume).reverse.map(m => f"${showMouse.show(m)}").mkString(";")
}
