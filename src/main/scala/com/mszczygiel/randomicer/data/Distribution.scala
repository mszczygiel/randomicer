package com.mszczygiel.randomicer.data

import cats.Show

import scala.math.abs

sealed case class Distribution(groups: Seq[Group]) {
  def penalty: Double = {
    val penalties = for {
      g1 <- groups
      g2 <- groups
    } yield abs(g1.mean - g2.mean) + abs(g1.median - g2.median)
    penalties.sum / 2.0
  }
}

object Distribution {

  implicit def showDistribution(implicit showGroup: Show[Group]): Show[Distribution] =
    (distribution: Distribution) => distribution.groups.zipWithIndex.map {
      case (g, i) => f"G${i + 1}: ${showGroup.show(g)}"
    }.mkString(scala.compat.Platform.EOL)
}
