package com.mszczygiel.randomicer.output

import cats.Show
import com.mszczygiel.randomicer.data.Distribution

object ConsoleOutput {
  def printDistribution(distribution: Distribution): Unit = {
    val distShow: Show[Distribution] = implicitly[Show[Distribution]]
    println(distShow.show(distribution))
  }
}
