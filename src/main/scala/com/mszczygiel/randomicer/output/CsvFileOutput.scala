package com.mszczygiel.randomicer.output

import java.nio.file.{Files, Paths, StandardOpenOption}

import scala.collection.JavaConverters._
import com.mszczygiel.randomicer.data.{Distribution, Group, Mouse}

object CsvFileOutput {
  private val Separator = ";"
  private def Symbols: Stream[String] = Stream.continually(Stream("_ _", "_ o", "o _", "o o", "_ oo", "oo _", "oo oo", "o oo", "oo o")).flatten

  def writeDistribution(path: String, distribution: Distribution): Unit = {
    val groupsLines = distribution.groups.zipWithIndex.flatMap{ case (g: Group, idx: Int) =>
      groupToLines(idx + 1, g)
    }
    Files.write(Paths.get(path), groupsLines.asJava, StandardOpenOption.CREATE)
  }

  private def groupToLines(idx: Int, group: Group): List[String] = {
    val header = List("Sym", "LP", "TV", "NT", "G" + idx.toString, "AVG", s"${group.mean}", "MED", s"${group.median}", "SD", s"${group.standardDeviation}").mkString(Separator)

    List(header) ++ miceToLines(group.mice.toList.sortBy(_.tumorVolume).reverse)
  }

  private def miceToLines(mice: List[Mouse]): List[String] = {
    mice.zip(Symbols).map { case (mouse, symbol) =>
      List(symbol, mouse.id, mouse.tumorVolume, mouse.tumors.size).mkString(Separator)
    }
  }
}
